<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ntersol
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#5e9aca" />
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- 
  <link rel="icon" type="image/x-icon" href="/favicon.ico">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5e9aca">
  <link rel="manifest" href="/manifest.json">
  -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<div id="page" class="site">
	<a class="skip-link screen-reader-text d-none" href="#content"><?php esc_html_e( 'Skip to content', 'ntersol' ); ?></a>

		<header id="masthead" class="site-header">
			<div class="container-fluid">
		<nav class="navbar navbar-expand-lg">
			  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="navbar-brand" href="/"><?php bloginfo( 'name' ); ?></a>
  
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main" aria-controls="main-navigation" aria-expanded="false" aria-label="Toggle navigation">
				<span class="hamburger"></span>
			  </button>
			  <div class="navbar-collapse collapse" id="navbar-main">
				  <div class="navbar-mobile">
				  <?php
								wp_nav_menu( array(
									'theme_location'  => 'menu-1',
									'menu_id'         => 'primary-menu',
									'depth'	          => 2, // 1 = no dropdowns, 2 = with dropdowns.
									'container'       => 'div',
									'container_class' => '',
									'container_id'    => 'main-navigation',
									'menu_class'      => 'navbar-nav mr-auto',
									'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
									'walker'          => new WP_Bootstrap_Navwalker(),
								) );
								?>
					</div>
					<div id="nav-mobile-bg"></div>
				</div>
				<!-- 
				<form class="form-inline my-2 my-lg-0">
				  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
				  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>
				-->
			</nav>
	</div>
		</header><!-- #masthead -->
	<div id="content" class="site-content">