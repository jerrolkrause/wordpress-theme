/***********************
 * Homepage
 ************************/
(function ($) {
    'use strict';

    // Declaring main variable
    var APP = {};

    // Predefined Variables
    var $window = $(window),
        $document = $(document);

    // Window load functions
    $window.on('load', function () {

    });

    // Document ready functions
    $document.on('ready', function () {

    });

    // Window load and resize functions
    $window.on('load resize', function () {

    });

})(jQuery);