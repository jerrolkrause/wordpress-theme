/***********************
 * App/global
 ************************/
(function ($) {
    'use strict';

    // Declaring main variable
    var APP = {};

    // Predefined Variables
    var $window = $(window),
        $document = $(document),
        $this = $(this),
        header = $('header'),
        navBg = $('#nav-mobile-bg'),
        scrollLast = 0; // Used by APP.headerSticky

    APP.headerSticky = function () {
        var st = $window.scrollTop();
        // If the window is at the top, remove classes
        if (st < 5) {
            header
                .removeClass('pinned')
                .removeClass('unpinned');
        }

        // Make sure they scroll more than delta
        if (Math.abs(scrollLast - st) <= 15) {
            return;
        }
        // If scrolling up
        if (st < scrollLast) {
            header
                .addClass('pinned')
                .removeClass('unpinned');
        } else {
            header
                .removeClass('pinned')
                .addClass('unpinned');;
        }
        scrollLast = st;
    }

    // Window load functions
    $window.on('load', function () {

    });

    // Window load functions
    $window.on('scroll', function () {
        APP.headerSticky();
    });

    // Document ready functions
    $(function () {
        // When the mobile background is clicked
        $('#nav-mobile-bg').on('click', function () {
            $('#navbar-main').collapse('hide');
        });
    });
    
    // Window load and resize functions
    $window.on('load resize', function () {

    });

})(jQuery);