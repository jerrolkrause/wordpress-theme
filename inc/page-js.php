<?php

function ntersol_page_js_meta_box() {
    add_meta_box(
        'page-js',
        __( 'Page JS', '@ntersol' ),
        'ntersol_page_js_meta_box_callback'
    );
}

add_action( 'add_meta_boxes', 'ntersol_page_js_meta_box' );

function ntersol_page_js_meta_box_callback( $post ) {

    // Add a nonce field so we can check for it later.
    wp_nonce_field( 'page_js_nonce', 'page_js_nonce' );

    $value = get_post_meta( $post->ID, '_page_js', true );
	echo '<p>Add a link to javascript file for this page only. The file must be uploaded first. </p>';
    echo '<input style="width:100%" id="page_js" name="page_js" value="' . esc_attr( $value ) . '"/><br/>';
	echo 'IE: <small><em>/wp-content/themes/ntersol/assets/site/js/pages/home.js?ver=1.0.0</em></small>';
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id
 */
function ntersol_save_page_js_meta_box_data( $post_id ) {

    // Check if our nonce is set.
    if ( ! isset( $_POST['page_js_nonce'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['page_js_nonce'], 'page_js_nonce' ) ) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    }
    else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */

    // Make sure that it is set.
    if ( ! isset( $_POST['page_js'] ) ) {
        return;
    }

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['page_js'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_page_js', $my_data );
}

add_action( 'save_post', 'ntersol_save_page_js_meta_box_data' );


function ntersol_page_add_js( $content ) {
    global $post;
    // retrieve the global notice for the current post
    $page_js = esc_attr( get_post_meta( $post->ID, '_page_js', true ) );
	if($page_js){
		echo '<script type="text/javascript" src="' . $page_js . '"></script>';
	}
}

add_filter( 'wp_footer', 'ntersol_page_add_js', 80 );