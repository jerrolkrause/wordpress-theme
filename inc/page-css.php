<?php

function ntersol_page_css_meta_box() {
    add_meta_box(
        'page-css',
        __( 'Page CSS', '@ntersol' ),
        'ntersol_page_css_meta_box_callback'
    );
}

add_action( 'add_meta_boxes', 'ntersol_page_css_meta_box' );

function ntersol_page_css_meta_box_callback( $post ) {

    // Add a nonce field so we can check for it later.
    wp_nonce_field( 'page_css_nonce', 'page_css_nonce' );
	
    $value = get_post_meta( $post->ID, '_page_css', true );
	echo '<p>Add inline styles for this page only, no &lt;style&gt; tag needed. </p>';
    echo '<textarea style="width:100%" id="page_css" name="page_css" rows="4">' . esc_attr( $value ) . '</textarea>';
	echo 'IE: <small><em>h1{color:red;}</em></small>';
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id
 */
function ntersol_save_page_css_meta_box_data( $post_id ) {

    // Check if our nonce is set.
    if ( ! isset( $_POST['page_css_nonce'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['page_css_nonce'], 'page_css_nonce' ) ) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    }
    else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */

    // Make sure that it is set.
    if ( ! isset( $_POST['page_css'] ) ) {
        return;
    }

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['page_css'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_page_css', $my_data );
}

add_action( 'save_post', 'ntersol_save_page_css_meta_box_data' );

function ntersol_page_css_before_post( $content ) {

    global $post;

    // retrieve the global notice for the current post
    $page_css = esc_attr( get_post_meta( $post->ID, '_page_css', true ) );
	if($page_css){
		echo '<style>' . $page_css . '</style>';
	}
	
}

add_filter( 'wp_head', 'ntersol_page_css_before_post' );