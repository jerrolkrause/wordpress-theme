Ntersol WordPress Theme
---------------
A rapid theme starter for WordPress.

Essential Plugins
---------------
HTML Editor Syntax Highlighter
https://wordpress.org/plugins/html-editor-syntax-highlighter/

W3 Total Cache
https://wordpress.org/plugins/w3-total-cache/

WP-SCSS
https://wordpress.org/plugins/wp-scss/