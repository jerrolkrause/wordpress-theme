<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ntersol
 */

?>

	</div><!-- #content -->
	
	<footer id="colophon" class="site-footer">
	<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-md-3">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("footer-col-1") ) : ?><?php endif;?>
				</div>
				<div class="col-12 col-md-3">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("footer-col-2") ) : ?><?php endif;?>
				</div>
				<div class="col-12 col-md-3">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("footer-col-3") ) : ?><?php endif;?>
				</div>
				<div class="col-12 col-md-3">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("footer-col-4") ) : ?><?php endif;?>
				</div>
			</div>
		<div class="site-info">
		</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->
	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
